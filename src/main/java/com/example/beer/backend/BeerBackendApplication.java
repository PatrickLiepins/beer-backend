package com.example.beer.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeerBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeerBackendApplication.class, args);
	}
}
